# Dat Zine Library Change Log

## 9 July 2018

You can now edit your branch details from the front-end _or_ the back-end.  So if you are the owner of the library, you'll see an edit button next to yr library name.  Click on that to edit yr details!

**Current Dream**: the styling on this is not the best.  I wanted it to look like a box lid sliding out to reveal the form beneath...but it doesn't look like that.  Also, I'd like all the text visible, but within the box still...so if you type a long name the box itself resizes to fit it.  Not quite sure how to do that yet!



