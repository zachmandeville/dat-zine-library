# Dat Zine Library

It's a zine library for the Chorus!

**You can see an example library here:**
`dat://dat-zine-library.hashbase.io`

**To get a personal history, and changelog, for this library code check out: `https://coolguy.website/projects/dat-zine-library`

# Directory Structure

The code is organized around the metaphor of a real-life zine library.  So you have all of the zines, the building the zines go in, and the volunteers that perform all the actions that make the whole operation run.
You also have the identity of this library branch, and what distinguishes it from other zine libraryies.

This repo is laid out the same:

- `branch/`Holds the name and details of this particular library branch.
- `building/` Holds the HTML and CSS components that make up the library's structure and look.
- `volunteers/` Holds the core javascript functions that handle when the site is loaded, a zine is added, a zine is edited, etc.
- `zines/` Holds the zines themselves, each in their own text file.

TODO: Each folder has its own README with more details.

# Customizing the library

If you are the librarian, you can edit any of these files to make your branch your own.  The level of customization depends on how much coding you wanna do.  

Here's what I recommend:
- Edit the branch details either by clicking edit in the front-end, or by editing the info.txt file in the `branch/` folder.
- Remove any zines you don't wanna carry by deleting them from the `zines/` folder.
- Add new zines either through the front end, or by making a new .txt file in the `zines/` folder.
- Customize the general look by going to `building/aesthetic/stylesheets/colors-and-fonts.css`
- Customize the whole styling by going to `building/aesthetic/stylesheets/main.css`
- Customize the structural code in `building/components` or `volunteers`.  This last step will require some more javascript knowledge.


The library is built using [choo](https://choo.io).  This is a cute framework for building interactive web apps.  Their homepage has an awesome tutorial on getting started.  To really customize this, though, you'll need to install all required modules and such to your local computer.  (see below)

# Installing

The simplest way to install this is to visit the library link in beaker, then fork it and add it to your local directory. 

Alternately, you can clone this from gitlab.
`git clone https://gitlab.com/zachmandeville/dat-zine-library.git`

Then:
```
cd dat-zine-library
npm install
npm run build
```

If you cloned it from gitlab, you'll need to add this folder to yr BeakerBrowser:

```
- Open Beaker
- Click the Hamburger Icon in the top Right
- choose Create New > From Folder
- select dat-zine-library
- Click the link in the top right to view it!
```

# Usage
 
To add a new dat zine:
You can do it through the add-zine button on the front end, or through the backend like so:

- right click and then 'view files'
- click on the 'zines' folder
- Click 'New'
- add a txt file named after your zine that looks like this: 
 
```
title: name of zine
----
url: dat:/ads;lkfajdsgklajglkah3j2kljakhdh32j.sha256
```

Click save and you should be good.  If not, lemme know!

