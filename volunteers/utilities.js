const _ = require('lodash')

module.exports = {inCollection, $, $$, toggleElements}

function inCollection (zine, param) {
  // Check whether zine has collections, and if so if any match the current
  // route parameter
  param = decodeURI(param)
  if (zine.collections) {
    return zine.collections.find(collection => _.toLower(collection) === param)
  } else return false
}

function $ (el, sel = false) {
  if (typeof sel === 'string') {
    // (el, sel)
    return el.querySelector(sel)
  }
  // (sel)
  return document.querySelector(el)
}

function $$ (el, sel = false) {
  if (typeof sel === 'string') {
    // (el, sel)
    return el.querySelectorAll(sel)
  }
  // (sel)
  return document.querySelectorAll(el)
}

function toggleElements (elements, className) {
  for (var element of elements) {
    element.classList.toggle(className)
  }
}
