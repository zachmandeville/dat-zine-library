const smarkt = require('smarkt')
const _ = require('lodash')

var archive = new DatArchive(window.location)

function store (state, emitter) {
  emitter.on('zine submitted', function (form) {
  // take form, convert it to object, then smarkt string, then write it to file.
    var details = getDetails(form)
    if (details.collections) { // if submission includes a collections entry, convert it to an array
      details.collections = toArray(details.collections)
    }
    var zineJson = JSON.stringify(details, null, 2)
    writeZineTxt(zineJson, emitter)
  })
}

function getDetails (form) {
  // take an html form an return it as an object
  var formData = new FormData(form)
  var data = {}
  for (var pair of formData.entries()) {
    data[pair[0]] = pair[1]
  }
  return data
}

function writeZineTxt (zine, emitter) {
  // take a smarkt text and write it as a .txt in zines folder,
  // then update the library
  zine = JSON.parse(zine)
  var fileName = zine.title.replace(/ /g, '-') + '.txt'
  var pleasantText = smarkt.stringify(zine)
  archive.writeFile(`zines/${fileName}`, pleasantText)
    .then(() => emitter.emit('update library', zine)) // handled by branchManager
}

function toArray (string) {
  // split a string by commas, remove all white space from each entry, and return the clean array
  var arr = string.split(',')
  arr = _.map(arr, _.trim)
  return arr
}

module.exports = store
