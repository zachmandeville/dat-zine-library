const choo = require('choo')

const library = require('./building/components/library')

const app = choo()

// Jobs to be done by the javascript volunteers
app.use(require('./volunteers/branchManager'))
app.use(require('./volunteers/addNewZines'))
app.use(require('./volunteers/displayBranchInfo'))

app.route('/', library)
app.route('#collections/:collection', library)
app.mount('body')
