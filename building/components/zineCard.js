const html = require('nanohtml')
const _ = require('lodash')
const button = require('./actionButtons')

const u = require('../../volunteers/utilities')

module.exports = (zine, state, emit) => {
  var id = zine.title.replace(/ /g, '-')
  return html`
    <li id='zine_${id}' class='front'>
    <div class='zine-card'>
     <div class='title'>
      <h1>${zine.title}</h1>
      ${button.editCard(state, zine)}
     </div>
     <div class='card-slots'>
      ${cardFront()}
      ${cardEdit()}
     </div>
    </div>
    </li>
    `

  function cardFront () {
    var url = _.trimEnd(zine.url, '/')
    var img = state.covers[url]
    return html`
      <div class='front'>
     <a href=${zine.url} title=${zine.title} target="_blank">
      <div class='details'>
      <img src=${img}/>
      <p>${zine.notes}</p>
      ${infoFromZine(zine, state)}
      </div>
    </a>
      </div>
      `
  }

  function cardEdit () {
    return html`
      <div class='edit inset-form no-display'>
      <form onsubmit=${submitZine}> 
      <label for='title'>Title:
      <input id='title-edit' 
        name='title' 
        value='${zine.title}' 
      />
      </label>
      <label for='url'>Dat URL:
      <input class='url-edit' 
        name='url' 
        type='text' 
        required 
        value='${zine.url}'
      />
      </label>
      <label for='notes'>Notes:</label> 
      <textarea class='notes-edit' 
        name='notes' 
        placeholder='where you found it, how it makes you feel, why you wrote it, whatever!'
      >${zine.notes}</textarea>
      <label for='collections'>Collections:</label>
      <textarea class='collections-edit'
        type='text'
        name='collections' 
        placeholder='where in your library does this belong? separate collections with commas.'
      > ${collectionsAsValue(zine.collections)}</textArea>
      <input type='submit' />
      </form>
      </div>
      `
  }

  function submitZine (e) {
    e.preventDefault()
    var zine = e.currentTarget
    emit('zine submitted', zine)
    toggleCards()
  }

  function toggleCards () {
    var id = zine.title.replace(/ /g, '-')
    var cards = u.$$(`#zine_${id} .front, #zine+${id} .edit`)
    u.toggleElements(cards, 'no-display')
  }
}

function infoFromZine (zine, state) {
  var trimmedUrl = _.trimEnd(zine.url, '/')
  var info = _.entries(state.info[trimmedUrl])
  if (info) {
    return html`
      <div id='zine-info'>
      <h1>Info From Zine</h1>
      ${renderInfo(info)}
      </div>
      `
  }
}

function renderInfo (info) {
  return info.map(pair => {
    return html`<p><strong>${pair[0]}:</strong> ${pair[1]}</p>`
  })
}

function collectionsAsValue (collections) {
  if (collections) {
    var collAsString = _.join(collections, ', ')
    var trimmedColl = _.trim(collAsString, ',')
    return trimmedColl
  }
}
