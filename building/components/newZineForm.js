var html = require('nanohtml')
var choo = require('choo')

module.exports = view

function view (emit) {
//I would like this to be form validated, but cannot figure out how to
// Do so, the regEx pattern matching doesn't seem to work...
  return html`
    <section id='add-zines'>
      <div id='new-zine-form' class='form-card transition'>
      <h1>Add A Zine</h1>
      <form onsubmit=${submitZine} class='outset-form'> 
      <label for='title'>Title:
      <input id='title' name='title' 
        placeholder='name of the zine' />
      </label>
      <label for='url'>Dat URL:
      <input id='url' name='url' 
        type='text' 
        required
        placeholder='the dat url' />
      </label>
      <label for='notes'>Notes:</label> <textarea id='new-zine-notes' name='notes' placeholder='how you found it, maybe yr feelings?'/></textarea>
      <label for='collections'>Collections:</label>
    <textarea type='text'name='collections' placeholder='cool zines, sweet zines, sexy zines?'></textarea>
      <input type='submit' />
      </form>
      </div>
    </section>
    `
  function submitZine (e) {
    e.preventDefault()
    var zine = e.currentTarget
    emit('zine submitted', zine)
  }
}
