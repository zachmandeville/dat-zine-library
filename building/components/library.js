const html = require('nanohtml')

const newZineForm = require('./newZineForm')
const shelving = require('./shelving')
const button = require('./actionButtons')
const branchPlacard = require('./branchPlacard')
const newBranchForm = require('./newBranchForm')
const collections = require('./collections')

module.exports = view

function view (state, emit) {
  return html`
    <body>
    ${branchPlacard(state, emit)}
    ${button.addZine(state)}
    ${newZineForm(emit)}
    ${newBranchForm()}
    ${collections(state)}
    ${shelving(state, emit)}
    </body>
    `
// newZineForm is a button for adding new zines
// shelving is the grid that shows all zines pulled from yr zines/ folder
// I am writing these notes here cos I dont' know how to leave comments in nanohtml code :( .
//  If anyone knows i'd appreciate the help!
}
