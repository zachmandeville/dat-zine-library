const html = require('nanohtml')

const u = require('../../volunteers/utilities')

const zineCard = require('./zineCard')

module.exports = (state, emit) => {
  return html`
    <ul id='zine-shelves'>
    ${state.library.map(listZines)}
    </ul>
   `

  function listZines (zine) {
    var param = state.params.collection
    var inCollection = u.inCollection(zine, param)
    if (param && !inCollection) {
      return // nothing
    } else {
      return html`
        ${zineCard(zine, state, emit)}
        `
    }
  }
}
