const html = require('nanohtml')

const utilities = require('../../volunteers/utilities')
const $ = utilities.$
const $$ = utilities.$$

module.exports = { addZine, makeBranch, editDetails, editCard, placardClose }

function addZine (state) {
  if (state.identity === 'librarian') {
    return html`
      <button id='add-button' onclick=${toggleForm}>Add Zine</button>
    `
  }
}

function makeBranch () {
  return html`
    <button id='branch-button' onclick=${toggleBranch}>Make A New Branch</button>
  `
}

function editDetails (id) {
  if (id === 'librarian') {
    return html`<button id='edit-form-button' onclick=${placardOpen}>edit</button>`
  } else {
    console.log('hi friend')
  }
}

function editCard (state, zine) {
  if (state.identity === 'librarian') {
    return html`<button class='edit-card-button' onclick=${() => toggleEdit(zine.title)}>edit</button>`
  }
}

function toggleEdit (title) {
  var id = title.replace(/ /g, '-')
  var cards = $$(`#zine_${id} .front, #zine_${id} .edit`)
  var editButton = $(`#zine_${id} .edit-card-button`)
  toggleElements(cards, 'no-display')
  changeWording(editButton, 'edit', 'cancel')
}

function placardOpen () {
  var placard = document.querySelector('#placard')
  var form = document.querySelector('#placard-form')
  placard.classList.add('hidden')
}

function placardClose () {
  var placard = document.querySelector('#placard')
  var form = document.querySelector('#placard-form')
  placard.classList.remove('hidden')
}

function toggleForm () {
  var addButton = document.getElementById('add-button')
  var form = document.getElementById('new-zine-form')

  form.classList.toggle('revealed')

  if (addButton.textContent === 'close') {
    addButton.textContent = 'Add Zine'
  } else {
    addButton.textContent = 'close'
  }
}

function toggleBranch () {
  var branchButton = document.querySelector('#branch-button')
  var form = document.querySelector('#new-branch-form')

  form.classList.toggle('revealed')

  if (branchButton.textContent === 'close') {
    branchButton.textContent = 'Make a New Branch!'
  } else {
    branchButton.textContent = 'close'
  }
}

function changeWording (item, word1, word2) {
// toggle an item's wording upon button click
  var word = item.textContent
  word === word1 ? word = word2 : word = word1
  item.textContent = word
}

function toggleElements (elements, className) {
  for (var element of elements) {
    element.classList.toggle(className)
  }
}
    
