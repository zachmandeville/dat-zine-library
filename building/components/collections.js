const html = require('nanohtml')

module.exports = (state, emit) => {
  if (state.collections) {
    return html`
      <section id ='collections'>
      <div>
      <h2>Collections</h2>
      <ul id='collections-list'>
      <li><a href='/' class='primary-li'>all</a></li>
      ${collectionsList(state.collections)}
      </ul>
      </div>
      <h3 id='collection-selection'>${state.params.collection}</h3>
      </section>
      `
  }

  function collectionsList (arr) {
    return arr.map(collection => {
      return html`
        <li><a href='#collections/${collection}'>${collection}</a></li>
        `
    })
  }
}
